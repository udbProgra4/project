create database PrograIV
use PrograIV

CREATE TABLE Usuario(
Username nvarchar(15) NOT NULL Primary key,
Email varchar(30) UNIQUE NOT NULL,
Pass varchar(10) NOT NULL,
Nombres varchar(80) NOT NULL,
Apellidos varchar(80) NOT NULL
)

create table Simulacion(
idSimulacion nvarchar (30) NOT NULL Primary Key, 
idUsuario nvarchar(15) NOT NULL,
FOREIGN KEY (idUsuario) REFERENCES Usuario(Username)
)

create table Protocolos(
idProtocolo INT IDENTITY(1, 1) NOT NULL primary key,
nombre varchar(30) not null,
distanciaAdministrativa int not null
)

create table Vertices(
idVertice INT IDENTITY(1, 1) NOT NULL primary key,
posX int not null,
posY int not null,
radio int not null,
nombre varchar(30) not null, 
idSimulacion nvarchar (30) NOT NULL, 
FOREIGN KEY (idSimulacion) REFERENCES Simulacion(idSimulacion)
)

create table Arcos(
idArco INT IDENTITY(1, 1) NOT NULL primary key,
vertice1 INT not null,
vertice2 INT not null,
idProtocolo INT not null,
idSimulacion nvarchar (30) NOT NULL, 
FOREIGN KEY (idProtocolo) REFERENCES Protocolos(idProtocolo),
FOREIGN KEY (idSimulacion) REFERENCES Simulacion(idSimulacion),
FOREIGN KEY (vertice1) REFERENCES Vertices(idVertice),
FOREIGN KEY (vertice2) REFERENCES Vertices(idVertice)
)

--Consultas
--select A.idArco, (SELECT nombre FROM Vertices AS V1 INNER JOIN Arcos AS Arc ON V1.idVertice = Arc.vertice1) AS Vertice1 , (SELECT nombre FROM Vertices AS V2 INNER JOIN Arcos AS Arc ON V2.idVertice = Arc.vertice2) AS Vertice2 ,P.nombre , P.distanciaAdministrativa FROM Arcos AS A INNER JOIN Simulacion As S ON A.idSimulacion = S.idSimulacion INNER JOIN Protocolos AS P ON A.idProtocolo = P.idProtocolo