﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorProtocoloEnrutamiento
{
    public partial class frmNuevoUsuario : Form
    {
        frmLogin padre;
        string IdSimulacion;

        public void generarIdSimulacion(string Idusuario){
            DateTime fecha = DateTime.Today;
            IdSimulacion = fecha.Year + "-" + fecha.Day + "-" + Idusuario;
        } 

        public frmNuevoUsuario(frmLogin frmpadre)
        {
            padre = frmpadre;
            InitializeComponent();
        }

        private void llBacktoLogin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
            padre.Show();
        }

        private void frmNuevoUsuario_Load(object sender, EventArgs e)
        {
            
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            generarIdSimulacion(txtUsuario.Text.Trim());
            insert.ingresoUsuario(txtUsuario.Text, txtEmail.Text, txtPass.Text, txtNombre.Text, txtApellido.Text);
            insert.ingresoSimulacion(IdSimulacion,txtUsuario.Text.Trim());
        }

        private void txtUsuario_Validating(object sender, CancelEventArgs e)
        {
            
        }

        private void txtPass_Validating(object sender, CancelEventArgs e)
        {
           
        }

        private void txtNombre_Validating(object sender, CancelEventArgs e)
        {
            
        }

        private void txtApellido_Validating(object sender, CancelEventArgs e)
        {

        }

        private void txtEmail_Validating(object sender, CancelEventArgs e)
        {

        }

        private void btnRegistrar_Validating(object sender, CancelEventArgs e)
        {

            /*
            if (txtUsuario.Text.Trim() == ""){
                epRegistroUsuario.SetError(txtUsuario, "El usuario es requerido");
                e.Cancel = true;
                return;
            }

            epRegistroUsuario.SetError(txtUsuario, "");

            
            if (txtPass.Text.Trim() == ""){
                epRegistroUsuario.SetError(txtPass, "La contraseña es requerida");
                e.Cancel = true;
                return;
            }
            epRegistroUsuario.SetError(txtPass, "");
            
            if (txtNombre.Text.Trim() == ""){
                epRegistroUsuario.SetError(txtNombre, "El nombre es requerido");
                e.Cancel = true;
                return;
            }else{
                foreach (var l in txtNombre.Text){
                    if (char.IsNumber(l)){
                        epRegistroUsuario.SetError(txtNombre, "El nombre no puede contener números");
                        e.Cancel = true;
                        return;
                    }
                }

            }
            epRegistroUsuario.SetError(txtNombre, "");


            if (txtApellido.Text.Trim() == ""){
                epRegistroUsuario.SetError(txtApellido, "El apellido es requerido");
                e.Cancel = true;
                return;
            }else{
                foreach (var l in txtApellido.Text){
                    if (char.IsNumber(l)){
                        epRegistroUsuario.SetError(txtApellido, "El apellido no puede contener números");
                        e.Cancel = true;
                        return;
                    }
                }

            }
            epRegistroUsuario.SetError(txtApellido, "");

            if (txtEmail.Text.Trim() == ""){
                epRegistroUsuario.SetError(txtEmail, "La email es requerido");
                e.Cancel = true;
                return;
            }
            epRegistroUsuario.SetError(txtEmail, "");

             */

        }
    }
}
