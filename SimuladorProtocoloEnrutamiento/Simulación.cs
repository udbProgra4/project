﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorProtocoloEnrutamiento
{
    public partial class frmSimulador : Form
    {
        int varCMS = 0;
        Grafos nGrafo = new Grafos();

        //variables e instancias asociadas a System.Drawing
        Image newImage = Image.FromFile("../../img/Router.png");
        Graphics g;

        //Configuración de Propiedades de estilo
        Font font = new Font("Arial", 10, FontStyle.Bold);
        Font fontTheme = new Font("Arial", 12, FontStyle.Bold);
        StringFormat sf = new StringFormat();
        Pen pincel = new Pen(Color.Black);
        Brush pincelString = new SolidBrush(Color.White);
        Brush pincelCircle = new SolidBrush(Color.DodgerBlue);
        Brush pincelTheme = new SolidBrush(Color.Red);


        int posX = 0;
        int posY = 0;

        public frmSimulador()
        {
            InitializeComponent();
        }

        private void frmSimulador_Load(object sender, EventArgs e){
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;
            nGrafo.vertices = select.SVertices();
            nGrafo.arcos = select.SArcos();
        }

        private void pbSimulacion_Paint(object sender, PaintEventArgs e)
        {
            g = e.Graphics;
            
            if (varCMS == 1){
                e.Graphics.DrawImage(newImage, posX, posY, 64, 64);
            }

            nGrafo.dibujarGrafo(newImage, g, font, pincelString, sf, pincel);
        }

        private void pbSimulacion_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left){
                if(varCMS == 1){
                    Clases.GlobalesRouter.pos = new Point(posX, posY);
                    Clases.GlobalesRouter.radio = 64;
                    frmRouter newRouter = new frmRouter();
                    newRouter.ShowDialog();
                    nGrafo.vertices = select.SVertices();
                    pbSimulacion.Invalidate();
                    varCMS = 0;
                }
            }            
            
            if (e.Button == System.Windows.Forms.MouseButtons.Right){
                if (varCMS == 0){
                    pbSimulacion.ContextMenuStrip = this.CMS;
                }
            }
        }

        private void crearRouterTStripMenuItem_Click(object sender, EventArgs e)
        {
            varCMS = 1;
        }

        private void crearEnlaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEnrutamiento newEnlace = new frmEnrutamiento();
            newEnlace.ShowDialog();

            nGrafo.arcos = select.SArcos();
            pbSimulacion.Invalidate();
            varCMS = 0;
            

        }

        private void pbSimulacion_MouseMove(object sender, MouseEventArgs e)
        {
            posX = e.Location.X; 
            posY = e.Location.Y;

            pbSimulacion.Invalidate();
            

        }

        private void eliminarEnlaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEnrutamiento newEnlace = new frmEnrutamiento();
            newEnlace.ShowDialog();

            nGrafo.arcos = select.SArcos();
            pbSimulacion.Invalidate();
            varCMS = 0;
        }

        private void eliminarRouterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EliminarRouter deleteVertice = new EliminarRouter();
            deleteVertice.ShowDialog();

            nGrafo.vertices = select.SVertices();
            nGrafo.arcos = select.SArcos();
            pbSimulacion.Invalidate();
            varCMS = 0;
        }


    }
}
