﻿namespace SimuladorProtocoloEnrutamiento
{
    partial class frmSimulador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbltema = new System.Windows.Forms.Label();
            this.pbSimulacion = new System.Windows.Forms.PictureBox();
            this.CMS = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.crearRouterTStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearEnlaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarEnlaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarRouterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pbSimulacion)).BeginInit();
            this.CMS.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbltema
            // 
            this.lbltema.AutoSize = true;
            this.lbltema.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltema.Location = new System.Drawing.Point(264, 11);
            this.lbltema.Name = "lbltema";
            this.lbltema.Size = new System.Drawing.Size(315, 20);
            this.lbltema.TabIndex = 0;
            this.lbltema.Text = "Simulación Protocolo de Enrutamiento";
            // 
            // pbSimulacion
            // 
            this.pbSimulacion.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pbSimulacion.Location = new System.Drawing.Point(12, 41);
            this.pbSimulacion.Name = "pbSimulacion";
            this.pbSimulacion.Size = new System.Drawing.Size(819, 426);
            this.pbSimulacion.TabIndex = 1;
            this.pbSimulacion.TabStop = false;
            this.pbSimulacion.Paint += new System.Windows.Forms.PaintEventHandler(this.pbSimulacion_Paint);
            this.pbSimulacion.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbSimulacion_MouseDown);
            this.pbSimulacion.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbSimulacion_MouseMove);
            // 
            // CMS
            // 
            this.CMS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearRouterTStripMenuItem,
            this.crearEnlaceToolStripMenuItem,
            this.eliminarEnlaceToolStripMenuItem,
            this.eliminarRouterToolStripMenuItem});
            this.CMS.Name = "CMS";
            this.CMS.Size = new System.Drawing.Size(156, 114);
            // 
            // crearRouterTStripMenuItem
            // 
            this.crearRouterTStripMenuItem.Name = "crearRouterTStripMenuItem";
            this.crearRouterTStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.crearRouterTStripMenuItem.Text = "Crear Router";
            this.crearRouterTStripMenuItem.Click += new System.EventHandler(this.crearRouterTStripMenuItem_Click);
            // 
            // crearEnlaceToolStripMenuItem
            // 
            this.crearEnlaceToolStripMenuItem.Name = "crearEnlaceToolStripMenuItem";
            this.crearEnlaceToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.crearEnlaceToolStripMenuItem.Text = "Crear Enlace";
            this.crearEnlaceToolStripMenuItem.Click += new System.EventHandler(this.crearEnlaceToolStripMenuItem_Click);
            // 
            // eliminarEnlaceToolStripMenuItem
            // 
            this.eliminarEnlaceToolStripMenuItem.Name = "eliminarEnlaceToolStripMenuItem";
            this.eliminarEnlaceToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.eliminarEnlaceToolStripMenuItem.Text = "Eliminar Enlace";
            this.eliminarEnlaceToolStripMenuItem.Click += new System.EventHandler(this.eliminarEnlaceToolStripMenuItem_Click);
            // 
            // eliminarRouterToolStripMenuItem
            // 
            this.eliminarRouterToolStripMenuItem.Name = "eliminarRouterToolStripMenuItem";
            this.eliminarRouterToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.eliminarRouterToolStripMenuItem.Text = "Eliminar Router";
            this.eliminarRouterToolStripMenuItem.Click += new System.EventHandler(this.eliminarRouterToolStripMenuItem_Click);
            // 
            // frmSimulador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 494);
            this.Controls.Add(this.pbSimulacion);
            this.Controls.Add(this.lbltema);
            this.Name = "frmSimulador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simulación Protocolo de Enrutamiento";
            this.Load += new System.EventHandler(this.frmSimulador_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbSimulacion)).EndInit();
            this.CMS.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbltema;
        private System.Windows.Forms.PictureBox pbSimulacion;
        private System.Windows.Forms.ContextMenuStrip CMS;
        private System.Windows.Forms.ToolStripMenuItem crearRouterTStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearEnlaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarEnlaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarRouterToolStripMenuItem;
    }
}

