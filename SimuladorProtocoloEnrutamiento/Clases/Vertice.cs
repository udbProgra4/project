﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace SimuladorProtocoloEnrutamiento
{
    class Vertice
    {
        int idVertice;

        public int IdVertice
        {
            get { return idVertice; }
            set { idVertice = value; }
        }

        Point pos;

        public Point Pos
        {
            get { return pos; }
            set { pos = value; }
        }

        int radio;

        public int Radio
        {
            get { return radio; }
            set { radio = value; }
        }

        string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }


        public Vertice() { }
    }
}
