﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace SimuladorProtocoloEnrutamiento
{
    class Grafos
    {
        public List<Vertice> vertices;
        public List<Arco> arcos;
        public Vertice[,] MatrizAdyacencia;

        public Grafos() {
            vertices = new List<Vertice>();
            arcos = new List<Arco>();
        }
        
        public void parVertices() {      
            foreach(Arco A in arcos){
                foreach(Vertice V in vertices){
                    if (A.Vertice1 == V.IdVertice){
                        A.V1 = V;
                    }
                    if (A.Vertice2 == V.IdVertice){
                        A.V2 = V;
                    }   
                }
            }
        }

        public void generarMatriz() {
            parVertices();
            MatrizAdyacencia = new Vertice[vertices.Count, vertices.Count];
        
        }

        public void dibujarGrafo(Image newImage, Graphics g, Font font, Brush pincel, StringFormat sf, Pen pen){
            foreach(Vertice v in vertices){
                g.DrawImage(newImage, v.Pos.X, v.Pos.Y, 64, 64);
                g.DrawString(v.Nombre, font, pincel, v.Pos.X + (v.Radio/2) , v.Pos.Y + v.Radio, sf);
            }

            parVertices();
            
            foreach(Arco A in arcos){

                int radio = A.V1.Radio;
                Point Ci = A.V1.Pos;
                Point Cf = A.V2.Pos;
                Point Cv1 = new Point();
                Point Cv2 = new Point();

                int direfenciaX = Cf.X - Ci.X;
                int direfenciaY = Cf.Y - Ci.Y;

                //Faltan relaciones y dar un poco más de pixeles cuando se sume el radio, o modificar el tamaño de la imagen

                if (Cf.X > Ci.X && Cf.Y > Ci.Y){
                    Cv1 = new Point(Ci.X + radio, Ci.Y + (radio / 2));
                    Cv2 = new Point(Cf.X, Cf.Y + (radio/2));
                }else if(Cf.X < Ci.X && Cf.Y < Ci.Y){
                    Cv2 = new Point(Cf.X + radio, Cf.Y + (radio / 2));
                    Cv1 = new Point(Ci.X, Ci.Y + (radio / 2));
                }else if(Cf.X < Ci.X && Cf.Y > Ci.Y){
                    Cv2 = new Point(Cf.X + radio, Cf.Y);
                    Cv1 = new Point(Ci.X, Ci.Y + (radio / 2));
                }else if (Cf.X > Ci.X && Cf.Y < Ci.Y){
                    Cv2 = new Point(Cf.X + radio, Cf.Y);
                    Cv1 = new Point(Ci.X, Ci.Y + (radio / 2));
                }


                g.DrawLine(pen, Cv1, Cv2);
                

            }


        }





    }
}
