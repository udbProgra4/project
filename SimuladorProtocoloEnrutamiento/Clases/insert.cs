﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SimuladorProtocoloEnrutamiento
{
    public class insert
    {
        public static void ingresoUsuario(string IdUsuario, string email, string pass, string nombre, string apellido ) {
            Conexion cn = new Conexion();
            try {
                cn.ConexionBd();
                cn.StrSqlQuery = "insert into Usuario (Username, Email, Pass, Nombres, Apellidos) values (@User, @Email, @pass, @Nombre, @Apellidos);";
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@User", SqlDbType.NVarChar).Value = IdUsuario;
                cn.ObjComando.Parameters.Add("@Email", SqlDbType.VarChar).Value = email;
                cn.ObjComando.Parameters.Add("@pass", SqlDbType.VarChar).Value = pass;
                cn.ObjComando.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = nombre;
                cn.ObjComando.Parameters.Add("@Apellidos", SqlDbType.VarChar).Value = apellido;
                cn.ObjComando.ExecuteNonQuery();
                cn.Desconexion();
            }catch{
                MessageBox.Show("Ha ocurrido un inconveniente en la creación del usuario");
            }
        } 


        public static void ingresoSimulacion(string idSimulacion, string IdUsuario) {
            Conexion cn = new Conexion();
            try {
                cn.ConexionBd();
                cn.StrSqlQuery = "insert into Simulacion (idSimulacion, idUsuario) values (@idSimulacion, @idUsuario);";
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@idSimulacion", SqlDbType.NVarChar).Value = idSimulacion;
                cn.ObjComando.Parameters.Add("@idUsuario", SqlDbType.NVarChar).Value = IdUsuario;
                cn.ObjComando.ExecuteNonQuery();
                cn.Desconexion();
            }catch{
                MessageBox.Show("Ha ocurrido un inconveniente en el registro de la simulación asociada al usuario");
            }
        }

        public static void ingresoProtocolo(string nombre, int distancia){
            Conexion cn = new Conexion();
            try{
                cn.ConexionBd();
                cn.StrSqlQuery = "insert into Protocolos (nombre, distanciaAdministrativa) values (@nombre, @dis);";
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@nombre", SqlDbType.NVarChar).Value = nombre;
                cn.ObjComando.Parameters.Add("@dis", SqlDbType.NVarChar).Value = distancia;
                cn.ObjComando.ExecuteNonQuery();
                cn.Desconexion();
            }catch{
                MessageBox.Show("Ha ocurrido un inconveniente en el registro del protocolo");
            }
        } 


        public static void ingresoArco(int idVertice1, int idVertice2, int idProtocolo, string idSimulacion){
            Conexion cn = new Conexion();
            try{
                cn.ConexionBd();
                cn.StrSqlQuery = "insert into Arcos (vertice1, vertice2, idProtocolo, idSimulacion) values (@idV1, @idV2, @idP, @idS);";
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@idV1", SqlDbType.Int).Value = idVertice1;
                cn.ObjComando.Parameters.Add("@idV2", SqlDbType.Int).Value = idVertice2;
                cn.ObjComando.Parameters.Add("@idP", SqlDbType.Int).Value = idProtocolo;
                cn.ObjComando.Parameters.Add("@idS", SqlDbType.NVarChar).Value = idSimulacion;
                cn.ObjComando.ExecuteNonQuery();
                cn.Desconexion();
            }catch{
                MessageBox.Show("Ha ocurrido un inconveniente en el registro de los arcos");
            }
        } 
        
        public static void ingresoRouter(int posX, int posY, int radio, string nombre, string idSimulacion){
            Conexion cn = new Conexion();
            try{
                cn.ConexionBd();
                cn.StrSqlQuery = "insert into Vertices (posX, posY, radio, nombre, idSimulacion) values (@PosX, @PosY, @Radio, @Nombre, @IdS);";
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@PosX", SqlDbType.Int).Value = posX;
                cn.ObjComando.Parameters.Add("@PosY", SqlDbType.Int).Value = posY;
                cn.ObjComando.Parameters.Add("@Radio", SqlDbType.Int).Value = radio;
                cn.ObjComando.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = nombre;
                cn.ObjComando.Parameters.Add("@IdS", SqlDbType.NVarChar).Value = idSimulacion;
                cn.ObjComando.ExecuteNonQuery();
                cn.Desconexion();
            }catch{
                MessageBox.Show("Ha ocurrido un inconveniente en el registro del router");
            }
        } 



    }
}
