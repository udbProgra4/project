﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace SimuladorProtocoloEnrutamiento
{
    class Arco
    {
        int idArco;

        public int IdArco
        {
            get { return idArco; }
            set { idArco = value; }
        }

        int vertice1;

        public Vertice V1;

        public int Vertice1
        {
            get { return vertice1; }
            set { vertice1 = value; }
        }

        int vertice2;

        public Vertice V2;

        public int Vertice2
        {
            get { return vertice2; }
            set { vertice2 = value; }
        }

        int idProtocolo;

        public int IdProtocolo
        {
            get { return idProtocolo; }
            set { idProtocolo = value; }
        }

        public Arco() { }

    }
}
