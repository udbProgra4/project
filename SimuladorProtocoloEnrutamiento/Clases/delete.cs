﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorProtocoloEnrutamiento
{
    class delete
    {
        public static void EliminarArco(int id){
            Conexion cn = new Conexion();
            try
            {
                cn.ConexionBd();
                cn.StrSqlQuery = "DELETE FROM Arcos where idArco=@Id AND idSimulacion=@idS";
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                cn.ObjComando.Parameters.Add("@idS", SqlDbType.NVarChar).Value = Clases.Globales.IdSimulacion;
                cn.ObjComando.ExecuteNonQuery();
                cn.ObjComando.Parameters.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un inconveniente al intentar eliminar el enlace");
            }
            finally
            {
                cn.Desconexion();
            }
        }

        public static void EliminarVertice(int id){
            Conexion cn = new Conexion();
            try
            {
                cn.ConexionBd();
                cn.StrSqlQuery = "DELETE FROM Arcos where idSimulacion=@idS AND (vertice1=@idVertice OR vertice2=@idVertice)";
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@idS", SqlDbType.NVarChar).Value = Clases.Globales.IdSimulacion;
                cn.ObjComando.Parameters.Add("@idVertice", SqlDbType.Int).Value = id;
                cn.ObjComando.ExecuteNonQuery();
                cn.ObjComando.Parameters.Clear();


                cn.StrSqlQuery = "DELETE FROM Vertices where idSimulacion=@idS AND idVertice=@idVertice";
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@idS", SqlDbType.NVarChar).Value = Clases.Globales.IdSimulacion;
                cn.ObjComando.Parameters.Add("@idVertice", SqlDbType.Int).Value = id;
                cn.ObjComando.ExecuteNonQuery();
                cn.ObjComando.Parameters.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un inconveniente al intentar eliminar el enlace" + ex.ToString());
            }
            finally
            {
                cn.Desconexion();
            }
        }


    }
}
