﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SimuladorProtocoloEnrutamiento
{
    class select
    {
        public static Boolean verificarCredenciales(string email, string pass) {
            Conexion cn = new Conexion();
            cn.ConexionBd();
            cn.StrSqlQuery = "select count(*) from Usuario where Email=@email AND Pass=@pass";
            
            try{
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@email", SqlDbType.VarChar).Value = email;
                cn.ObjComando.Parameters.Add("@pass", SqlDbType.VarChar).Value = pass;
                int exis = (int)cn.ObjComando.ExecuteScalar();
                cn.ObjComando.Parameters.Clear();
                
                if(exis == 1){

                    cn.StrSqlQuery = "select * from Usuario where Email=@email AND Pass=@pass";
                    cn.ObjComando.CommandType = System.Data.CommandType.Text;
                    cn.ObjComando.CommandText = cn.StrSqlQuery;
                    cn.ObjComando.Parameters.Add("@email", SqlDbType.VarChar).Value = email;
                    cn.ObjComando.Parameters.Add("@pass", SqlDbType.VarChar).Value = pass;
                    cn.ObjReader = cn.ObjComando.ExecuteReader();
                    cn.ObjComando.Parameters.Clear();

                    while (cn.ObjReader.Read()){
                        Clases.Globales.Username = cn.ObjReader["Username"].ToString();
                        Clases.Globales.Nombre = cn.ObjReader["Nombres"].ToString();
                        Clases.Globales.Apellido = cn.ObjReader["Apellidos"].ToString();
                    }

                    return true;
                }else{
                    return false;
                }
 
            }catch (Exception ex){
                return false;
            }finally{
                cn.Desconexion();
            }
        }

        //Variables globales
        public static void obtenerDatos(string idUsuario) {
            Conexion cn = new Conexion();
            cn.ConexionBd();
            cn.StrSqlQuery = "SELECT * FROM Simulacion where idUsuario=@Id";

            try
            {
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@Id", SqlDbType.VarChar).Value = idUsuario;
                cn.ObjReader = cn.ObjComando.ExecuteReader();
                cn.ObjComando.Parameters.Clear();
                while (cn.ObjReader.Read())
                {
                    Clases.Globales.IdSimulacion = cn.ObjReader["idSimulacion"].ToString();
                }
            }catch{
                MessageBox.Show("Ha ocurrido un inconveniente al recuperar la información");
            }finally{
                cn.Desconexion();
            }
        }

        //DataGridView

         public static List<Protocolo> RProtocolos(){
             Protocolo objProtocolo;
             List<Protocolo> lstProtocolos = new List<Protocolo>();
             Conexion cn = new Conexion();
             cn.ConexionBd();
             cn.StrSqlQuery = "SELECT * FROM Protocolos";

            try
            {
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjReader = cn.ObjComando.ExecuteReader();
                while (cn.ObjReader.Read())
                {
                    objProtocolo = new Protocolo();
                    objProtocolo.IdProtocolo = cn.ObjReader["idProtocolo"].ToString();
                    objProtocolo.Nombre = cn.ObjReader["nombre"].ToString();
                    objProtocolo.DAdministrativa = int.Parse(cn.ObjReader["distanciaAdministrativa"].ToString());
                    lstProtocolos.Add(objProtocolo);
                }
            }catch (Exception ex){
                MessageBox.Show("Ha ocurrido un inconveniente al recuperar la información" + ex);
            }finally{
                cn.Desconexion();
            }
            return lstProtocolos;
        }
        
        public static List<infoArco> RArcos(){
            infoArco objArco;
            List<infoArco> lstInfoArco = new List<infoArco>();
            Conexion cn = new Conexion();
            cn.ConexionBd();
            cn.StrSqlQuery = "select A.idArco, A.vertice1, A.vertice2, (SELECT nombre FROM Vertices AS V1 INNER JOIN Arcos AS Arc1 ON V1.idVertice = Arc1.vertice1) AS Vertice1 , (SELECT nombre FROM Vertices AS V2 INNER JOIN Arcos AS Arc ON V2.idVertice = Arc.vertice2) AS Vertice2 , P.nombre , P.distanciaAdministrativa FROM Arcos AS A INNER JOIN Simulacion As S ON A.idSimulacion = S.idSimulacion INNER JOIN Protocolos AS P ON A.idProtocolo = P.idProtocolo WHERE S.idSimulacion=@idS";

            try
            {
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@idS", SqlDbType.VarChar).Value = Clases.Globales.IdSimulacion;
                cn.ObjReader = cn.ObjComando.ExecuteReader();
                while (cn.ObjReader.Read())
                {
                    objArco = new infoArco();
                    objArco.IdArco = int.Parse(cn.ObjReader["idArco"].ToString());
                    objArco.IdVertice1 = int.Parse(cn.ObjReader["vertice1"].ToString());
                    objArco.IdVertice2 = int.Parse(cn.ObjReader["vertice2"].ToString());
                    objArco.NombreVertice1 = cn.ObjReader["Vertice1"].ToString();
                    objArco.NombreVertice2 = cn.ObjReader["Vertice2"].ToString();
                    objArco.NombreProtocolo = cn.ObjReader["nombre"].ToString();
                    objArco.Distancia = int.Parse(cn.ObjReader["distanciaAdministrativa"].ToString());
                    lstInfoArco.Add(objArco);
                }
            }catch (Exception ex){
                MessageBox.Show("Ha ocurrido un inconveniente al recuperar la información" + ex);
            }finally{
                cn.Desconexion();
            }
            return lstInfoArco;
        }

        public static List<Routers> RRouters(){
            Routers objRouter;
            List<Routers> lstRouter = new List<Routers>();
            Conexion cn = new Conexion();
            cn.ConexionBd();
            cn.StrSqlQuery = "select * from Vertices WHERE idSimulacion=@idS";

            try
            {
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@idS", SqlDbType.VarChar).Value = Clases.Globales.IdSimulacion;
                cn.ObjReader = cn.ObjComando.ExecuteReader();
                while (cn.ObjReader.Read())
                {
                    objRouter = new Routers();
                    objRouter.IdVertice = int.Parse(cn.ObjReader["idVertice"].ToString());
                    objRouter.Nombre = cn.ObjReader["nombre"].ToString();
                    lstRouter.Add(objRouter);
                }
            }catch (Exception ex){
                MessageBox.Show("Ha ocurrido un inconveniente al recuperar la información" + ex);
            }finally{
                cn.Desconexion();
            }
            return lstRouter;
        }

        //Lista vertices y arco
        public static List<Vertice> SVertices(){
             Vertice objVertice;
             List<Vertice> lstVertices = new List<Vertice>();
             Conexion cn = new Conexion();  
             cn.ConexionBd();
             cn.StrSqlQuery = "SELECT * FROM Vertices Where idSimulacion=@Id ORDER BY idVertice";

            try
            {
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@Id", SqlDbType.NVarChar).Value = Clases.Globales.IdSimulacion;
                cn.ObjReader = cn.ObjComando.ExecuteReader();
                cn.ObjComando.Parameters.Clear();

                while (cn.ObjReader.Read())
                {
                    objVertice = new Vertice();
                    objVertice.IdVertice = int.Parse(cn.ObjReader["idVertice"].ToString());
                    objVertice.Pos = new Point(int.Parse(cn.ObjReader["posX"].ToString()), int.Parse(cn.ObjReader["posY"].ToString()));
                    objVertice.Radio = int.Parse(cn.ObjReader["radio"].ToString());
                    objVertice.Nombre = cn.ObjReader["nombre"].ToString();
                    lstVertices.Add(objVertice);
                }
            }catch (Exception ex){
                MessageBox.Show("Ha ocurrido un inconveniente al recuperar la información" + ex);
            }finally{
                cn.Desconexion();
            }
            return lstVertices;
        }
        
        public static List<Arco> SArcos(){
            Arco objArcos;
            List<Arco> lstArcos = new List<Arco>();
            Conexion cn = new Conexion();  
            cn.ConexionBd();
            cn.StrSqlQuery = "SELECT * FROM Arcos Where idSimulacion=@Id ORDER BY vertice1";

            try
            {
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjComando.Parameters.Add("@Id", SqlDbType.NVarChar).Value = Clases.Globales.IdSimulacion;
                cn.ObjReader = cn.ObjComando.ExecuteReader();
                cn.ObjComando.Parameters.Clear();

                while (cn.ObjReader.Read())
                {
                    objArcos = new Arco();
                    objArcos.IdArco = int.Parse(cn.ObjReader["idArco"].ToString());
                    objArcos.Vertice1 = int.Parse(cn.ObjReader["vertice1"].ToString());
                    objArcos.Vertice2 = int.Parse(cn.ObjReader["vertice2"].ToString());
                    objArcos.IdProtocolo = int.Parse(cn.ObjReader["idProtocolo"].ToString());
                    lstArcos.Add(objArcos);
                }
            }catch (Exception ex){
                MessageBox.Show("Ha ocurrido un inconveniente al recuperar la información" + ex);
            }finally{
                cn.Desconexion();
            }
            return lstArcos;
        }




        //Combobox Routers

        public static void datoProtocolos(ComboBox cb){
            Conexion cn = new Conexion();
            cn.ConexionBd();
            cn.StrSqlQuery = "SELECT idProtocolo, nombre FROM Protocolos";
            try{
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjReader = cn.ObjComando.ExecuteReader();
                cb.ValueMember = "Value";
                cb.DisplayMember = "Text";

                while (cn.ObjReader.Read()){
                    cb.Items.Add(new item { Text = cn.ObjReader["nombre"].ToString(), Value = cn.ObjReader["idProtocolo"].ToString() });
                }

            }catch (Exception){
                MessageBox.Show("Ha ocurrido un inconveniente al recuperar la información");
            }finally{
                cn.Desconexion();
            }
        }
        
        public static void datoRouters(ComboBox cb){
            Conexion cn = new Conexion();
            cn.ConexionBd();
            cn.StrSqlQuery = "SELECT idVertice, nombre FROM Vertices";
            try{
                cn.ObjComando.CommandType = System.Data.CommandType.Text;
                cn.ObjComando.CommandText = cn.StrSqlQuery;
                cn.ObjReader = cn.ObjComando.ExecuteReader();
                cb.ValueMember = "Value";
                cb.DisplayMember = "Text";

                while (cn.ObjReader.Read()){
                    cb.Items.Add(new item { Text = cn.ObjReader["nombre"].ToString(), Value = cn.ObjReader["idVertice"].ToString() });
                }

            }catch (Exception){
                MessageBox.Show("Ha ocurrido un inconveniente al recuperar la información");
            }finally{
                cn.Desconexion();
            }
        }



    }
}
