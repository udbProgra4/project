﻿using SimuladorProtocoloEnrutamiento.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorProtocoloEnrutamiento.Clases
{
    class Kruskal : IKruskal
    {
        IList<Arco> IKruskal.Solve(IList<Arco> graph, out int totalCost)
        {
            QuickSort(graph, 0, graph.Count - 1);
            IList<Arco> solvedGraph = new List<Arco>();
            totalCost = 0;

            foreach (Arco ed in graph)
            {
                //Necesitamos obtener si es el padre
                //Vertice vert1 = ed.V1.
                //Vertice vert2 = ed.V2.
                /*
                if (vert1.Nombre != vert2.Nombre)
                {
                    totalCost += ed.Cost;
                    Vertex.Join(root1, root2);
                    solvedGraph.Add(ed);
                }
                */
            }

            return solvedGraph;
        }

        private void QuickSort(IList<Arco> graph, int left, int right)
        {
            int i, j, x;
            i = left; j = right;
            //Necesitamos recuperar el peso del protocolo
            //x = graph[(left + right) / 2].[Peso del protocolo];
            /*
            do
            {
                while ((graph[i].Cost < x) && (i < right))
                {
                    i++;
                }

                while ((x < graph[j].Cost) && (j > left))
                {
                    j--;
                }

                if (i <= j)
                {
                    Edge y = graph[i];
                    graph[i] = graph[j];
                    graph[j] = y;
                    i++;
                    j--;
                }
            }
            while (i <= j);

            if (left < j)
            {
                QuickSort(graph, left, j);
            }

            if (i < right)
            {
                QuickSort(graph, i, right);
            }
            */
        }
    }
}
