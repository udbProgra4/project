﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorProtocoloEnrutamiento
{
    class Conexion{
        string cadenaConexion = "Data Source=FAM\\ADMINJ;Initial Catalog=PrograIV;User ID=sa;Password=123456789";

        public string CadenaConexion{
            get { return cadenaConexion; }
            set { cadenaConexion = value; }
        }
        String strSqlQuery = "";

        public String StrSqlQuery{
            get { return strSqlQuery; }
            set { strSqlQuery = value; }
        }

        SqlConnection objConexion;

        public SqlConnection ObjConexion{
            get { return objConexion; }
            set { objConexion = value; }
        }
        SqlCommand objComando;

        public SqlCommand ObjComando{
            get { return objComando; }
            set { objComando = value; }
        }

        SqlDataReader objReader;

        public SqlDataReader ObjReader{
            get { return objReader; }
            set { objReader = value; }
        }

        public void ConexionBd(){
            try
            {
                objConexion = new SqlConnection(CadenaConexion);
                objConexion.Open();
                objComando = new SqlCommand();
                objComando.Connection = objConexion;
            }
            catch (Exception)
            {
                

            }
        }

        public void Desconexion(){
            if (objConexion.State != ConnectionState.Closed)
            {
                objConexion.Close();
                objConexion.Dispose();
                objComando.Dispose();
            }
        }

    }
}
