﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorProtocoloEnrutamiento
{
    class Bd{}


    public class item
    {
        private string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        private string value;

        public string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
    }

    public class Protocolo{

        string idProtocolo;

        public string IdProtocolo
        {
            get { return idProtocolo; }
            set { idProtocolo = value; }
        }

        string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        int dAdministrativa;

        public int DAdministrativa
        {
            get { return dAdministrativa; }
            set { dAdministrativa = value; }
        }

    }

    public class infoArco {
        int idArco;

        public int IdArco
        {
            get { return idArco; }
            set { idArco = value; }
        }

        int idVertice1;

        public int IdVertice1
        {
            get { return idVertice1; }
            set { idVertice1 = value; }
        }

        int idVertice2;

        public int IdVertice2
        {
            get { return idVertice2; }
            set { idVertice2 = value; }
        }

        string nombreVertice1;

        public string NombreVertice1
        {
            get { return nombreVertice1; }
            set { nombreVertice1 = value; }
        }

        string nombreVertice2;

        public string NombreVertice2
        {
            get { return nombreVertice2; }
            set { nombreVertice2 = value; }
        }

        string nombreProtocolo;

        public string NombreProtocolo
        {
            get { return nombreProtocolo; }
            set { nombreProtocolo = value; }
        }

        int distancia;

        public int Distancia
        {
            get { return distancia; }
            set { distancia = value; }
        }
    }

    public class Routers{
        int idVertice;

        public int IdVertice
        {
            get { return idVertice; }
            set { idVertice = value; }
        }

        string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
    }
}
