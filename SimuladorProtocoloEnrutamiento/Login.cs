﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorProtocoloEnrutamiento
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            if(select.verificarCredenciales(txtEmail.Text, txtPass.Text)){

                select.obtenerDatos(Clases.Globales.Username);

                frmMenu menu = new frmMenu(this);
                menu.Show();
                this.Hide();
                txtEmail.Clear();
                txtPass.Clear();

            }else{
                MessageBox.Show("Datos incorrectos, asegurese de estar registrado");
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        private void llRegistrarCuenta_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmNuevoUsuario newUser = new frmNuevoUsuario(this);
            newUser.Show();
            this.Hide();

            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmNuevoUsuario newUser = new frmNuevoUsuario(this);
            newUser.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
