﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorProtocoloEnrutamiento
{
    public partial class frmMenu : Form
    {
        frmLogin padre;
        public frmMenu(frmLogin login){
            padre = login;
            
            InitializeComponent();
        }

        private void MDI_Load(object sender, EventArgs e)
        {

        }

        private void TSMProtocolo_Click(object sender, EventArgs e)
        {
            frmProtocolos Protocolos = new frmProtocolos();
            Protocolos.MdiParent = this;
            Protocolos.Show();
        }

        private void TSMRouter_Click(object sender, EventArgs e)
        {

        }

        private void TSMEnlace_Click(object sender, EventArgs e)
        {
            frmEnrutamiento Enrutamiento = new frmEnrutamiento();
            Enrutamiento.MdiParent = this;
            Enrutamiento.Show();

        }

        private void TSMSimulacion_Click(object sender, EventArgs e)
        {
            frmSimulador simnulacion = new frmSimulador();
            simnulacion.MdiParent = this;
            simnulacion.Show();
        }

        private void TSMCerrarSesion_Click(object sender, EventArgs e)
        {
            this.Close();
            padre.Show();
        }

    }
}
