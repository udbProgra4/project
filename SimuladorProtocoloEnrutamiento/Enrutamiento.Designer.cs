﻿namespace SimuladorProtocoloEnrutamiento
{
    partial class frmEnrutamiento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbEnrutamiento = new System.Windows.Forms.GroupBox();
            this.btnEnlazar = new System.Windows.Forms.Button();
            this.cbProtocolo = new System.Windows.Forms.ComboBox();
            this.lblPE = new System.Windows.Forms.Label();
            this.cbRouter2 = new System.Windows.Forms.ComboBox();
            this.lblRouter2 = new System.Windows.Forms.Label();
            this.cbRouter1 = new System.Windows.Forms.ComboBox();
            this.lblRouter1 = new System.Windows.Forms.Label();
            this.lblenlace = new System.Windows.Forms.Label();
            this.gbDatos = new System.Windows.Forms.GroupBox();
            this.dgvDatos = new System.Windows.Forms.DataGridView();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.gbEnrutamiento.SuspendLayout();
            this.gbDatos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).BeginInit();
            this.SuspendLayout();
            // 
            // gbEnrutamiento
            // 
            this.gbEnrutamiento.Controls.Add(this.btnEliminar);
            this.gbEnrutamiento.Controls.Add(this.btnEnlazar);
            this.gbEnrutamiento.Controls.Add(this.cbProtocolo);
            this.gbEnrutamiento.Controls.Add(this.lblPE);
            this.gbEnrutamiento.Controls.Add(this.cbRouter2);
            this.gbEnrutamiento.Controls.Add(this.lblRouter2);
            this.gbEnrutamiento.Controls.Add(this.cbRouter1);
            this.gbEnrutamiento.Controls.Add(this.lblRouter1);
            this.gbEnrutamiento.Controls.Add(this.lblenlace);
            this.gbEnrutamiento.Location = new System.Drawing.Point(12, 12);
            this.gbEnrutamiento.Name = "gbEnrutamiento";
            this.gbEnrutamiento.Size = new System.Drawing.Size(289, 178);
            this.gbEnrutamiento.TabIndex = 0;
            this.gbEnrutamiento.TabStop = false;
            this.gbEnrutamiento.Text = "Enrutamiento";
            // 
            // btnEnlazar
            // 
            this.btnEnlazar.Location = new System.Drawing.Point(11, 128);
            this.btnEnlazar.Name = "btnEnlazar";
            this.btnEnlazar.Size = new System.Drawing.Size(96, 29);
            this.btnEnlazar.TabIndex = 8;
            this.btnEnlazar.Text = "Enlazar";
            this.btnEnlazar.UseVisualStyleBackColor = true;
            this.btnEnlazar.Click += new System.EventHandler(this.btnEnlazar_Click);
            // 
            // cbProtocolo
            // 
            this.cbProtocolo.FormattingEnabled = true;
            this.cbProtocolo.Location = new System.Drawing.Point(145, 99);
            this.cbProtocolo.Name = "cbProtocolo";
            this.cbProtocolo.Size = new System.Drawing.Size(123, 21);
            this.cbProtocolo.TabIndex = 7;
            // 
            // lblPE
            // 
            this.lblPE.AutoSize = true;
            this.lblPE.Location = new System.Drawing.Point(7, 102);
            this.lblPE.Name = "lblPE";
            this.lblPE.Size = new System.Drawing.Size(132, 13);
            this.lblPE.TabIndex = 6;
            this.lblPE.Text = "Protocolo de Enrutamiento";
            // 
            // cbRouter2
            // 
            this.cbRouter2.FormattingEnabled = true;
            this.cbRouter2.Location = new System.Drawing.Point(147, 67);
            this.cbRouter2.Name = "cbRouter2";
            this.cbRouter2.Size = new System.Drawing.Size(121, 21);
            this.cbRouter2.TabIndex = 5;
            // 
            // lblRouter2
            // 
            this.lblRouter2.AutoSize = true;
            this.lblRouter2.Location = new System.Drawing.Point(144, 51);
            this.lblRouter2.Name = "lblRouter2";
            this.lblRouter2.Size = new System.Drawing.Size(51, 13);
            this.lblRouter2.TabIndex = 4;
            this.lblRouter2.Text = "Router 2:";
            // 
            // cbRouter1
            // 
            this.cbRouter1.FormattingEnabled = true;
            this.cbRouter1.Location = new System.Drawing.Point(10, 67);
            this.cbRouter1.Name = "cbRouter1";
            this.cbRouter1.Size = new System.Drawing.Size(121, 21);
            this.cbRouter1.TabIndex = 3;
            // 
            // lblRouter1
            // 
            this.lblRouter1.AutoSize = true;
            this.lblRouter1.Location = new System.Drawing.Point(8, 51);
            this.lblRouter1.Name = "lblRouter1";
            this.lblRouter1.Size = new System.Drawing.Size(51, 13);
            this.lblRouter1.TabIndex = 1;
            this.lblRouter1.Text = "Router 1:";
            // 
            // lblenlace
            // 
            this.lblenlace.AutoSize = true;
            this.lblenlace.Location = new System.Drawing.Point(7, 24);
            this.lblenlace.Name = "lblenlace";
            this.lblenlace.Size = new System.Drawing.Size(127, 13);
            this.lblenlace.TabIndex = 0;
            this.lblenlace.Text = "Par de Routers a enlazar:";
            // 
            // gbDatos
            // 
            this.gbDatos.Controls.Add(this.dgvDatos);
            this.gbDatos.Location = new System.Drawing.Point(307, 12);
            this.gbDatos.Name = "gbDatos";
            this.gbDatos.Size = new System.Drawing.Size(524, 178);
            this.gbDatos.TabIndex = 1;
            this.gbDatos.TabStop = false;
            this.gbDatos.Text = "Datos";
            // 
            // dgvDatos
            // 
            this.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatos.Location = new System.Drawing.Point(6, 16);
            this.dgvDatos.Name = "dgvDatos";
            this.dgvDatos.Size = new System.Drawing.Size(512, 150);
            this.dgvDatos.TabIndex = 0;
            this.dgvDatos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDatos_CellContentClick);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(124, 128);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(96, 29);
            this.btnEliminar.TabIndex = 9;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // frmEnrutamiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 206);
            this.Controls.Add(this.gbDatos);
            this.Controls.Add(this.gbEnrutamiento);
            this.Name = "frmEnrutamiento";
            this.Text = "Conexión";
            this.Load += new System.EventHandler(this.Enrutamiento_Load);
            this.gbEnrutamiento.ResumeLayout(false);
            this.gbEnrutamiento.PerformLayout();
            this.gbDatos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbEnrutamiento;
        private System.Windows.Forms.ComboBox cbRouter1;
        private System.Windows.Forms.Label lblRouter1;
        private System.Windows.Forms.Label lblenlace;
        private System.Windows.Forms.Button btnEnlazar;
        private System.Windows.Forms.ComboBox cbProtocolo;
        private System.Windows.Forms.Label lblPE;
        private System.Windows.Forms.ComboBox cbRouter2;
        private System.Windows.Forms.Label lblRouter2;
        private System.Windows.Forms.GroupBox gbDatos;
        private System.Windows.Forms.DataGridView dgvDatos;
        private System.Windows.Forms.Button btnEliminar;
    }
}