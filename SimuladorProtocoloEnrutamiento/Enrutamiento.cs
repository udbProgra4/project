﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorProtocoloEnrutamiento
{
    public partial class frmEnrutamiento : Form
    {
        private int id = 0;

        public frmEnrutamiento()
        {
            InitializeComponent();
        }

        public static void comboBx(ComboBox cbRouter1, ComboBox cbRouter2, ComboBox cbProtocolo){
            if (cbRouter1.Items.Count > 0){
                cbRouter1.SelectedIndex = 0;
            }

            if (cbRouter2.Items.Count > 0){
                cbRouter2.SelectedIndex = 0;
            }

            if (cbProtocolo.Items.Count > 0){
                cbProtocolo.SelectedIndex = 0;
            }
        }

        private void Enrutamiento_Load(object sender, EventArgs e)
        {
            btnEliminar.Enabled = false;

            select.datoProtocolos(cbProtocolo);
            select.datoRouters(cbRouter1);
            select.datoRouters(cbRouter2);
            comboBx(cbRouter1, cbRouter2, cbProtocolo);


            dgvDatos.ReadOnly = true;
            dgvDatos.DataSource = select.RArcos();
            dgvDatos.Columns[0].Visible = false;
            dgvDatos.Columns[1].Visible = false;
            dgvDatos.Columns[2].Visible = false;
            dgvDatos.Columns[3].Width = 130;
            dgvDatos.Columns[4].Width = 130;
            dgvDatos.Columns[5].Width = 130;
            dgvDatos.Columns[6].Width = 150;
            dgvDatos.Columns[3].HeaderText = "Nombre Vertice1";
            dgvDatos.Columns[4].HeaderText = "Nombre Vertice2";
            dgvDatos.Columns[5].HeaderText = "Nombre Protocolo";
            dgvDatos.Columns[6].HeaderText = "Distancia Administrativa";

        }

        private void btnEnlazar_Click(object sender, EventArgs e)
        {
            if(cbRouter1.Items.Count > 0 && cbRouter2.Items.Count > 0 && cbProtocolo.Items.Count > 0){

                item itemComboRouter1 = (item)cbRouter1.SelectedItem;
                item itemComboRouter2 = (item)cbRouter2.SelectedItem;
                item itemComboProtocolo = (item)cbProtocolo.SelectedItem;

                if (int.Parse(itemComboRouter1.Value) == int.Parse(itemComboRouter2.Value)){
                    MessageBox.Show("No puede enlazar al mismo router");
                }else{
                    insert.ingresoArco(int.Parse(itemComboRouter1.Value), int.Parse(itemComboRouter2.Value), int.Parse(itemComboProtocolo.Value), Clases.Globales.IdSimulacion);
                    this.Close();
                }

            }else{
                MessageBox.Show("No hay parametros suficientes para enlazar");
            }
        }

        private void dgvDatos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                id = int.Parse(dgvDatos.Rows[e.RowIndex].Cells[0].Value.ToString());
               
                btnEliminar.Enabled = true;
                btnEnlazar.Enabled = false;
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            delete.EliminarArco(id);
            btnEliminar.Enabled = false;
            btnEnlazar.Enabled = true;
            this.Close();
        }
    }
}
