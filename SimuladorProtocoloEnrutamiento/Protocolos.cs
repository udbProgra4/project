﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorProtocoloEnrutamiento
{
    public partial class frmProtocolos : Form
    {
        public frmProtocolos()
        {
            InitializeComponent();
        }

        private void frmProtocolos_Load(object sender, EventArgs e)
        {
            dgvDatos.ReadOnly = true;
            dgvDatos.DataSource = select.RProtocolos();
            dgvDatos.Columns[0].Visible = false;
            dgvDatos.Columns[1].Width = 200;
            dgvDatos.Columns[2].Width = 200;
            dgvDatos.Columns[1].HeaderText = "Nombre Protocolo";
            dgvDatos.Columns[2].HeaderText = "Distancia Administrativa";
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            insert.ingresoProtocolo(txtNombre.Text, (int) nudDistancia.Value);
            dgvDatos.DataSource = select.RProtocolos();
        }
    }
}
