﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorProtocoloEnrutamiento
{
    public partial class frmRouter : Form
    {
        public frmRouter()
        {
            InitializeComponent();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            insert.ingresoRouter(Clases.GlobalesRouter.pos.X, Clases.GlobalesRouter.pos.Y, Clases.GlobalesRouter.radio, txtNombreRouter.Text, Clases.Globales.IdSimulacion);
            this.Close();
        }

        private void frmRouter_Load(object sender, EventArgs e)
        {

        }
    }
}
