﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorProtocoloEnrutamiento
{
    public partial class EliminarRouter : Form
    {
        private int id = 0;

        public EliminarRouter()
        {
            InitializeComponent();
        }

        private void EliminarRouter_Load(object sender, EventArgs e)
        {
            btnEliminar.Enabled = false;
            txtNombreRouter.Enabled = false;

            dgvDatos.ReadOnly = true;
            dgvDatos.DataSource = select.RRouters();
            dgvDatos.Columns[0].Visible = false;
            dgvDatos.Columns[1].Width = 300;
            dgvDatos.Columns[1].HeaderText = "Nombre Router";
        }

        private void dgvDatos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                id = int.Parse(dgvDatos.Rows[e.RowIndex].Cells[0].Value.ToString());
                txtNombreRouter.Text = dgvDatos.Rows[e.RowIndex].Cells[1].Value.ToString();
                btnEliminar.Enabled = true;
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            delete.EliminarVertice(id);
            this.Close();
        }
    }
}
