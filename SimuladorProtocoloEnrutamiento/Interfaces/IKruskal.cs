﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorProtocoloEnrutamiento.Interfaces
{
    interface IKruskal
    {
        IList<Arco> Solve(IList<Arco> graph, out int totalCost);
    }
}
