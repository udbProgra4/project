# SIMULADOR DE ENVIO DE PAQUETES

Este es un proyecto realizado con fines educativos en el marco del desarrollo de la asignatura: Programación IV

### TECNOLOGÍAS IMPLEMENTADAS

* [System.Drawing](https://msdn.microsoft.com/es-es/library/system.drawing(v=vs.110).aspx) - libreria grafica

### REQUERIMIENTO

* Servidor de base de datos SQLSERVER
* Modificar conexión del proyecto

### CONTRIBUYENTES

  - Jonathan Armando Martínez Javier
  - Francisco Alejandro Paz Tejada
  - Manuel Francisco Guandique Lovo
  - César Alfredo Ventura Gamero

### License
MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

**Free Software, Hell Yeah!**